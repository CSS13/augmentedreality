package dk.cs.au.css13;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.Matrix4;

import com.badlogic.gdx.Input;

import java.util.ArrayList;

public class ArgumentedReality extends ApplicationAdapter 
{

    private PerspectiveCamera cam;
    private ModelBatch modelBatch;
    private ArrayList<ModelInstance> instances;
    private Environment environment;
    private CameraInputController camController;

    private Matrix4 mBRotMatrix;

    float camera_distance = 5;


    @Override
    public void create () 
    {
        modelBatch = new ModelBatch();

        cam = new PerspectiveCamera(67, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        cam.position.set(new Vector3(1f,1f,1f).setLength(camera_distance));
        cam.lookAt(0,0,0);
        cam.near = 1f;
        cam.far = 300f;
        cam.update();

        // Camera yaw, pitch using mouse.
        camController = new CameraInputController(cam);
        Gdx.input.setInputProcessor(camController);

        Material matRed = new Material(ColorAttribute.createDiffuse(Color.RED));
        Material matGreen = new Material(ColorAttribute.createDiffuse(Color.GREEN));
        Material matBlue = new Material(ColorAttribute.createDiffuse(Color.BLUE));
        Material matYellow = new Material(ColorAttribute.createDiffuse(Color.YELLOW));
        Material matGray = new Material(ColorAttribute.createDiffuse(Color.GRAY));

        ModelBuilder modelBuilder = new ModelBuilder();

        Vector3 origin = new Vector3(0f,0f,0f);
        Vector3 vecX = new Vector3(2f,0f,0f);
        Vector3 vecY = new Vector3(0f,2f,0f);
        Vector3 vecZ = new Vector3(0f,0f,2f);

        Model arrowX = modelBuilder.createArrow(origin, vecX, matRed, Usage.Position | Usage.Normal);
        Model arrowY = modelBuilder.createArrow(origin, vecY, matGreen, Usage.Position | Usage.Normal);
        Model arrowZ = modelBuilder.createArrow(origin, vecZ, matBlue, Usage.Position | Usage.Normal);

        // TODO: Use the same Model cube for all cubes
        Model centerBox = modelBuilder.createBox(1f, 1f, 1f, matYellow, Usage.Position | Usage.Normal);
        Model movingBox = modelBuilder.createBox(0.5f, 0.5f, 0.5f, matBlue, Usage.Position | Usage.Normal);
        Model smallBox = modelBuilder.createBox(0.5f, 0.5f, 0.5f, matGray, Usage.Position | Usage.Normal);

        instances = new ArrayList<ModelInstance>();

        instances.add(new ModelInstance(arrowX));
        instances.add(new ModelInstance(arrowY));
        instances.add(new ModelInstance(arrowZ));
        instances.add(new ModelInstance(centerBox));
        instances.add(new ModelInstance(movingBox));
        instances.add(new ModelInstance(smallBox));

        environment = new Environment();
        environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
        environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));

        mBRotMatrix = new Matrix4();
        mBRotMatrix.rotate(0f, 1f, 0f, 0.25f);
        instances.get(4).transform.translate(2.5f,0f,0f);
    }

    // A time variable
    int t = 0;

    // Which cube are we focusing
    int focus = 0;
    long last_focus_change = 0;

    @Override
    public void render () 
    {
        // Mouse camera movement
        camController.update();

        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        // Render everything
        modelBatch.begin(cam);
        modelBatch.render(instances, environment);
        modelBatch.end();

        // Update our transforms
        instances.get(3).transform.rotate(0f, 1f, 0f, 0.5f);

        instances.get(4).transform.mulLeft(mBRotMatrix);

        t++;
        instances.get(5).transform
            // Start of with identity
            .idt()
            // Rotate around Y
            .rotate(0f,1f,0f,0.25f*t)
            // Translate out the rotation direction
            .translate(2.5f,0f,0f)
            // Rotate around Y (at the rotation direction translation point)
            .rotate(0f,1f,0f,0.1f*t)
            // Rotate out the new rotation direction
            .translate(1f,0f,0f)
            ;

        // Ellipse translates
        /*
        // Hypotrochoid ellipse (special case when R=2r)
        instances.get(5).transform
            .idt()
            .rotate(0f,1f,0f,1f*t)
            .translate(3f,0f,0f)
            .rotate(0f,1f,0f,-2f*t)
            .translate(1.5f,0f,0f)
            ;

        // Parametric translation
        float alfa = 1;
        float beta = 2;
        instances.get(5).transform
            .idt()
            .translate(alfa * (float) Math.cos(t * 0.1f), 0f, beta * (float) Math.sin(t * 0.1f));
        */

        // Zooming (using UP/Down instead of mouse wheel)
        //
        // Scope to hide distance_change_direction
        // TODO: Should be a function really
        {
            int distance_change_direction = 0;
            // Figure out which way to change camera distance
            if(Gdx.input.isKeyPressed(Input.Keys.UP))
                distance_change_direction = -1;
            if(Gdx.input.isKeyPressed(Input.Keys.DOWN))
                distance_change_direction = 1;
            // Change the distance
            camera_distance = camera_distance + distance_change_direction * 0.1f;
            if(camera_distance < 0)
                camera_distance = 0;

            // Set camera distance
            // NOTE: If we're at (0,0,0) we have no vector to set the length of
            if(cam.position.isZero())
                cam.position.set(new Vector3(1f,1f,1f).setLength(camera_distance));
            else
                cam.position.set(cam.position.setLength(camera_distance));
        }

        // Focusing (using Left/Right)
        //
        // Scope to hide focus_change_direction
        // TODO: Should be a function really
        {
            // Figure out which way to affect the focus
            int focus_change_direction = 0;
            if(Gdx.input.isKeyJustPressed(Input.Keys.LEFT))
                focus_change_direction = -1;
            if(Gdx.input.isKeyJustPressed(Input.Keys.RIGHT))
                focus_change_direction = 1;
            // Change the focus
            focus = focus + focus_change_direction;
            if(focus > 2)
                focus = 0;
            if(focus < 0)
                focus = 2;
        }

        // Update the camera each frome (to track translation changes, ect).
        Vector3 focus_translate = instances.get(3+focus).transform.getTranslation(new Vector3(0f, 0f, 0f));
        // TODO: Do we really need this line below? - Ask the TA
        // focus_translate.y = 1;
        cam.lookAt(focus_translate);
        cam.up.set(0,1,0);
        cam.update();
    }

    @Override
    public void resize(int width, int height) 
    {
        cam.viewportWidth = Gdx.graphics.getWidth();
        cam.viewportHeight = Gdx.graphics.getHeight();
        cam.update();
    }

    @Override
    public void pause() {
    }

    @Override
    public void dispose () 
    {
    }
}
