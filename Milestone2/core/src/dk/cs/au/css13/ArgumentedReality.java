package dk.cs.au.css13;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.Matrix4;

import org.opencv.core.*;
import org.opencv.core.Mat;
import org.opencv.calib3d.Calib3d;
import org.opencv.core.Core;
import org.opencv.videoio.VideoCapture;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDouble;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.MatOfPoint3f;
import org.opencv.core.Size;
import org.opencv.core.Point3;

import java.util.ArrayList;

// TODO: Play with canny edge
// TODO: libgdx window size = camera resolution
// TODO: Depth buffer resolution = 32
// TODO: Length of chess fields = 26mm? - We're currently missing sizes
// TODO: Bonus, compute camera intrinsics

public class ArgumentedReality extends ApplicationAdapter 
{
    // Video capture + image matrices
    private VideoCapture cap;
    private Mat image;
    private Mat chess_image;

    // 3D Models
	private ModelBatch modelBatch;
    private Model model;
	private ArrayList<ModelInstance> instances;

    // Camera and environment
	private Environment environment;
    private PerspectiveOffCenterCamera cam;
  
	@Override
	public void create () 
	{
        // Setup the video capture device and output image matrices
        cap = new VideoCapture();
        image = new Mat();
        chess_image = new Mat();
        // Open the video capture and grab a frame
		cap.open(0);
		cap.read(image);
        // Test that the frame was valid
		while(image.type() == 0)
		{
			System.err.println("Image type invalid?");
            System.exit(1);
		}
        // Show the rendering windows
        UtilAR.imShow("raw_image", image);
        UtilAR.imShow("chess_image", image);
        // Setup the environment
		environment = new Environment();
		environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
		environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));
        // Setup 3D rendering structure
		modelBatch = new ModelBatch();
        instances = new ArrayList<ModelInstance>();
        // Setup the camera
		cam = new PerspectiveOffCenterCamera();
        
        Mat intrinsics = UtilAR.getDefaultIntrinsics(Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
        cam.setByIntrinsics(intrinsics, Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
		cam.position.set(new Vector3(5f,5f,5f));
		cam.lookAt(0f, 0f, 0f);
		cam.near = 1f;
		cam.far = 500f;
		cam.update();
        // Build our pretty yellow overlay box
		ModelBuilder modelBuilder = new ModelBuilder();
		model = modelBuilder.createBox(1f, 1f, 1f,
				new Material(ColorAttribute.createDiffuse(Color.YELLOW)),
                Usage.Position | Usage.Normal);
	}
  
	@Override
	public void render () 
	{
        // Capture a new image
        cap.read(image);
        // Render the raw image
        UtilAR.imShow("raw_image", image);

        // We're using a 6x5 board, it has 5x4 inner corners
        Size board = new Size(5,4);
        MatOfPoint2f corners = new MatOfPoint2f();
        // Try to find the chess board pattern
        boolean patternfound = Calib3d.findChessboardCorners(image, board, corners);
        // If the pattern was found, update 'found' our image
        if(patternfound)
        {
            // Render the chess board image
            chess_image = image.clone();
            Calib3d.drawChessboardCorners(chess_image, board, corners, patternfound);
            UtilAR.imShow("chess_image", chess_image);

            // Mapping size, should always be 1:1
            float cell_size = 1f;

            // Add the rendering squares (we want to actually render 6x5, not 5x4).
            for(int i = 0; i < board.height+1; ++i)
            {
                for(int j = 0; j < board.width+1; ++j)
                {
                    // Only render on 'black' (== 1 would be white).
                    if((i+j)%2 == 0) 
                    {
                        instances.add(new ModelInstance(model, j*cell_size-0.5f, 0.5f, i*cell_size-0.5f));
                    }
                }
            }

            // Setup the points for solvePnP
            Point3[] points = new Point3[(int)board.height*(int)board.width];
            for(int i = 0; i < board.height; ++i)
            {
                for(int j = 0; j < board.width; ++j)
                {
                    points[j+(i*(int)board.width)] = new Point3(j*cell_size, 0.0f, i*cell_size);
                }
            }

            // Wrap our solvePnP points for delivery.
            MatOfPoint3f points3d = new MatOfPoint3f(points);

            // Use solvePnP to get rotation and translation matrixes for the camera.
            Mat rmat = new Mat();
            Mat tvec = new Mat();
            Calib3d.solvePnP(points3d, corners, 
                    UtilAR.getDefaultIntrinsics(Gdx.graphics.getWidth(),Gdx.graphics.getHeight()),
                    UtilAR.getDefaultDistortionCoefficients(), rmat, tvec);
            // Adjust the camera according to rotation and translation
            UtilAR.setCameraByRT(rmat, tvec, cam);

            // Render the overlayed image
            UtilAR.imDrawBackground(image);
            modelBatch.begin(cam);
            modelBatch.render(instances, environment);
            modelBatch.end();
        }
	}
  
	@Override
	public void resize(int width, int height) 
	{
		cam.viewportWidth = width;
		cam.viewportHeight = height;
		cam.update();
	}

	@Override
	public void pause() 
	{
	}
	
	@Override
	public void dispose () 
	{
        cap.release();
	}
}
