package dk.cs.au.css13;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.ModelLoader;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.loader.ObjLoader;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.imageio.ImageIO;

import org.opencv.calib3d.Calib3d;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfDouble;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.MatOfPoint3f;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point3;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;

public class ArgumentedReality extends ApplicationAdapter 
{
    // Video capture + image matrices
    private VideoCapture cap;
    private Mat image;
    private Mat bw_image;
    private Mat thres_image;
    private Mat contour_image;
    private Mat polygon_image;
    private Mat cross_image;

    // 3D Models
	private ModelBatch modelBatch;
	private Model model;
	// instances is an array of arrays, because we have multiple worlds, one on each marker
    private Array<Array<ModelInstance>> instances;
	
    // Cameras and environment
	private Environment environment;
	// cams is an array, because we have multiple worlds, one on each marker
	private ArrayList<PerspectiveOffCenterCamera> cams;

	@Override
	public void create() 
    {
        // Load the native opencv library
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        // Setup the video capture device and output image matrices
        cap = new VideoCapture();
        image = new Mat();
       	bw_image = new Mat();
       	thres_image = new Mat();
       	contour_image = new Mat();
       	polygon_image = new Mat();
       	cross_image = new Mat();
        // Open the video capture and grab a frame
        cap.open(0);
        cap.read(image);
        // Test that the frame was valid
        while(image.type() == 0)
        {
            System.err.println("Image type invalid?");
            System.exit(1);
        }
        // Setup the environment
		environment = new Environment();
		environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
		environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));
        // Setup 3D rendering structure
		modelBatch = new ModelBatch();
        instances = new Array<Array<ModelInstance>>();
        cams = new ArrayList<PerspectiveOffCenterCamera>();
	}
	
    // Setup the camera
	public PerspectiveOffCenterCamera make_camera()
	{
        PerspectiveOffCenterCamera c = new PerspectiveOffCenterCamera();
        Mat intrinsics = UtilAR.getDefaultIntrinsics(Gdx.graphics.getWidth(),Gdx.graphics.getHeight());

        c.setByIntrinsics(intrinsics, Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
		c.position.set(new Vector3(5f,5f,5f));
		c.lookAt(0f, 0f, 0f);
		c.near = 1f;
		c.far = 500f;
		c.update();
		cams.add(c);

        return c;
	}

	@Override
	public void render() 
    {
        // Clean up various bits
        cams.clear();
        instances.clear();

        // Capture a new image
        cap.read(image);

        //--------------------------
		// a) Create a binary image.
        //--------------------------
		// Grayscale it
		Imgproc.cvtColor(image, bw_image, Imgproc.COLOR_BGR2GRAY);
		//UtilAR.imShow("B/W", bw_image);

		// Threshold it
		Imgproc.threshold(bw_image, thres_image, 70.0f, 255.0f, Imgproc.THRESH_BINARY);
		//UtilAR.imShow("THRESHOLD", thres_image);
		
        //----------------------
		// b) Find the contours.
        //----------------------
		List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
		Imgproc.findContours(thres_image, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
        /*
        contour_image = image.clone();
		Imgproc.drawContours(contour_image, contours, -1, new Scalar(0,0,255));
		UtilAR.imShow("CONTOUR-COLOR", contour_image);
		*/

        //----------------------------------
		// c) Create approximative polygons.
        //----------------------------------
		List<MatOfPoint> polygons = new ArrayList<MatOfPoint>();
		for(MatOfPoint contour : contours)
		{
			MatOfPoint2f thisContour2f = new MatOfPoint2f();
		    MatOfPoint approxContour = new MatOfPoint();
		    MatOfPoint2f approxContour2f = new MatOfPoint2f();
			
		    contour.convertTo(thisContour2f, CvType.CV_32FC2);
		    Imgproc.approxPolyDP(thisContour2f, approxContour2f, 5, true);
		    approxContour2f.convertTo(approxContour, CvType.CV_32S);
			
			polygons.add(approxContour);
		}
        /*
        polygon_image = image.clone();
		Imgproc.drawContours(polygon_image, polygons, -1, new Scalar(0,255,0));
		UtilAR.imShow("POLY-COLOR", polygon_image);
		*/

        //-----------------------------------------------------
		// d) Discard polygons which are not marker candidates.
        //-----------------------------------------------------
        // Check that we're provided rectangles and that they are convex.
		List<MatOfPoint> squares = new ArrayList<MatOfPoint>();
		for(MatOfPoint polygon : polygons)
		{
			if(polygon.size().height == 4 && Imgproc.isContourConvex(polygon)) {
				squares.add(polygon);
			}
		}
        // Sort the squares by size, largest first
        Collections.sort(squares, new Comparator<MatOfPoint>() 
        {
            public int compare(MatOfPoint p1, MatOfPoint p2) 
            {
                double p1area = Imgproc.contourArea(p1);
                double p2area = Imgproc.contourArea(p2);
                // TODO:
                // Return p1area - p2area ?
                if(p1area > p2area)
                    return -1;
                else if(p1area < p2area)
                    return 1;
                else
                    return 0;
            }
        });
		// Let's only get squares which are comparatively big
		List<MatOfPoint> areasquares = new ArrayList<MatOfPoint>();
		if(squares.isEmpty() == false)
		{
			double biggest = Imgproc.contourArea(squares.get(0));
			for(MatOfPoint square : squares)
			{
				double current = Imgproc.contourArea(square);
				if(current > 0.5*biggest)
				{
					areasquares.add(square);
				}
			}
		}
		
        // TODO: WAT
		List<MatOfPoint> crosspsquares = new ArrayList<MatOfPoint>();
		if(squares.isEmpty() == false)
		{
			for(MatOfPoint square : areasquares)
			{
				double[] corner1 = square.get(0, 0);
				double[] corner2 = square.get(1, 0);
				double[] corner3 = square.get(2, 0);
				double a = corner1[0]-corner2[0];
				double x = corner3[0]-corner2[0];
				double b = corner1[1]-corner2[1];
				double y = corner3[1]-corner2[1];
				double crossp = a*y-b*x;
				if(crossp < 0)
				{
					crosspsquares.add(square);
				}

			}
		}
        //-----------------------------------------------------------------
		// e) For each marker candidate draw the four edges and/or corners.
        //      -- Display the result image.
        //-----------------------------------------------------------------
        /*
        cross_image = image.clone();    
        Imgproc.drawContours(cross_image, crosspsquares, -1, new Scalar(255,0,0));
        UtilAR.imShow("SQUARE-COLOR", cross_image);
        */
		
		int i = 0;
		for(MatOfPoint mat_square : crosspsquares)
		{
            //UtilAR.imClose("OUTPUT-COLOR" + i);
            // Setup loop variables
			i++;
			instances.add(new Array<ModelInstance>());
			PerspectiveOffCenterCamera cam = make_camera();

            // Early exit
            //if(i > 3) break;
    
			MatOfPoint2f square = new MatOfPoint2f();
			mat_square.convertTo(square, CvType.CV_32FC2);
			
            //-------------------------------------------------------------------------------------
			// f) Use the PnP solver to render the 3D coordinate system onto the marker candidates.
            //-------------------------------------------------------------------------------------
			Point3[] points = { 
                new Point3(0,0,0),
                new Point3(0,0,1),
                new Point3(1,0,1),
                new Point3(1,0,0)
            };
			MatOfPoint3f points3d = new MatOfPoint3f(points);

			Mat rvec = new Mat();
			Mat tvec = new Mat();
            Calib3d.solvePnP(points3d, square, UtilAR.getDefaultIntrinsics(Gdx.graphics.getWidth(),Gdx.graphics.getHeight()),
                    UtilAR.getDefaultDistortionCoefficients(), rvec, tvec);

            UtilAR.setCameraByRT(rvec, tvec, cam);
	
            //------------------------------------------------------------------------------------    
			// g) Unwarp the content of the found marker candidate and display the unwarped image.
            //------------------------------------------------------------------------------------
			List<Point> homepoints = new ArrayList<Point>();
			homepoints.add(new Point(0,0));
			homepoints.add(new Point(image.width(),0));
			homepoints.add(new Point(image.width(),image.height()));
			homepoints.add(new Point(0,image.height()));
	
			MatOfPoint2f homopoints = new MatOfPoint2f();
			homopoints.fromList(homepoints);
			
			Mat M = Calib3d.findHomography(square, homopoints);
			Mat dest = new Mat();
			Imgproc.warpPerspective(image, dest, M, image.size());
			
			UtilAR.imShow("OUTPUT-COLOR" + i, dest);
			
        /*
			try
			{
				MatOfByte bytemat = new MatOfByte();
                Imgcodecs.imencode(".jpg", image, bytemat);
				byte[] bytes = bytemat.toArray();
				InputStream in = new ByteArrayInputStream(bytes);
				BufferedImage barCodeBufferedImage = ImageIO.read(in);
				
                /*
				LuminanceSource source = new BufferedImageLuminanceSource(barCodeBufferedImage);
				BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
				Reader reader = new QRCodeReader();
				Result result = reader.decode(bitmap);
		
				//System.out.println("QR-Code text is " + result.getText());
				String text = result.getText();
				
                ModelBuilder modelBuilder = new ModelBuilder();
				// giant IF to determine the right marker
				if(text.equals("1"))
				{
					model = modelBuilder.createArrow(0.5f, 0f, 0.5f, 0f, 0f, 0.5f,
						0.1f, 0.2f, 100, 1,
						new Material(ColorAttribute.createDiffuse(Color.RED)), Usage.Position | Usage.Normal);
					instances.get(i-1).add(new ModelInstance(model));
				}
				else if(text.equals("2"))
				{
					model = modelBuilder.createArrow(0.5f, 0f, 0.5f, 0f, 0f, 0.5f,
						0.1f, 0.2f, 100, 1,
						new Material(ColorAttribute.createDiffuse(Color.YELLOW)), Usage.Position | Usage.Normal);
					instances.get(i-1).add(new ModelInstance(model));
				}
				else if(text.equals("3"))
				{
					model = modelBuilder.createArrow(0.5f, 0f, 0.5f, 0f, 0f, 0.5f,
						0.1f, 0.2f, 100, 1,
						new Material(ColorAttribute.createDiffuse(Color.GREEN)), Usage.Position | Usage.Normal);
					instances.get(i-1).add(new ModelInstance(model));
				}
				else
				{
					System.out.println("INFO: QR-Code Message outside ifs");
					System.out.println("INFO: QR-Code Message:" + text);
				}
                */
            /*
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
            */
		}

        // Render the raw image
		UtilAR.imDrawBackground(image);
		// Render all the different worlds onto it
		for(int j=0; j<i; j++)
		{
            // Loop through each camera, and using it draw the instaces on it
			modelBatch.begin(cams.get(j));
			modelBatch.render(instances.get(j), environment);
			modelBatch.end();
		}
	}

	@Override
	public void resize(int width, int height) 
    {
	}
	
	@Override
	public void pause() 
    {
	}

	@Override
	public void dispose() 
	{
		cap.release();
	}
	
	@Override
	public void resume() 
    {
	}
}
