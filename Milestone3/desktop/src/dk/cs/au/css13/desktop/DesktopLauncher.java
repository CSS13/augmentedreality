package dk.cs.au.css13.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import dk.cs.au.css13.ArgumentedReality;

import org.opencv.core.Core;

public class DesktopLauncher 
{
	public static void main (String[] arg) 
	{
		System.out.println(System.getProperty("java.library.path"));
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		//System.loadLibrary("opencv_java310");
        //System.load("/home/skeen/Desktop/AR/Milestone2/desktop/jar/libopencv_java310.so");

		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        //config.width = 1600;
        //config.height = 1024;
		new LwjglApplication(new ArgumentedReality(), config);
	}
}
