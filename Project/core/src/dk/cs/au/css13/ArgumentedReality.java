package dk.cs.au.css13;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.ModelLoader;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.loader.ObjLoader;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import javax.imageio.ImageIO;

import java.awt.image.DataBufferByte;

import org.opencv.calib3d.Calib3d;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfDouble;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.MatOfPoint3f;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point3;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;

import com.google.zxing.common.BitMatrix;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.LuminanceSource;
import com.google.zxing.Reader;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeReader;

import com.badlogic.gdx.utils.UBJsonReader;

import com.badlogic.gdx.graphics.g3d.loader.G3dModelLoader;

/******
 * 
 * TODO: Fix multiple instances of same square
 * 
 ******/

public class ArgumentedReality extends ApplicationAdapter 
{
    // Video capture + image matrices
    private VideoCapture cap;
    private Mat image;
    private Mat bw_image;
    private Mat thres_image;
    private Mat contour_image;
    private Mat polygon_image;
    private Mat cross_image;

    // 3D Models
	private ModelBatch modelBatch;
	private Model model;
	// instances is an array of arrays, because we have multiple worlds, one on each marker
    private Array<Array<ModelInstance>> instances;
	
    // Cameras and environment
	private Environment environment;
	// cams is an array, because we have multiple worlds, one on each marker
	private ArrayList<PerspectiveOffCenterCamera> cams;

    // 3D models
    private Model arrowX;
    private Model arrowY;
    private Model arrowZ;

    // Chess pieces
    private Map<String, Model> chess_pieces = new HashMap<String, Model>();

    // Set vectors
    private final Vector3 origin = new Vector3(0f,0f,0f);
    private final Vector3 vecX   = new Vector3(2f,0f,0f);
    private final Vector3 vecY   = new Vector3(0f,2f,0f);
    private final Vector3 vecZ   = new Vector3(0f,0f,2f);

    // Material colors
    private Material matRed = new Material(ColorAttribute.createDiffuse(Color.RED));
    private Material matGreen = new Material(ColorAttribute.createDiffuse(Color.GREEN));
    private Material matBlue = new Material(ColorAttribute.createDiffuse(Color.BLUE));
    private Material matYellow = new Material(ColorAttribute.createDiffuse(Color.YELLOW));
    private Material matGray = new Material(ColorAttribute.createDiffuse(Color.GRAY));

    // Camera resolution
    private float cam_width = 1600;		//Default 640
    private float cam_height = 1200;	//Default 480

    private void loadChessPiece(String path, String piece_name)
    {
        UBJsonReader jsonReader = new UBJsonReader();
		ModelLoader loader = new G3dModelLoader(jsonReader);

        //System.out.println(new File(path).getAbsolutePath());

        Model piece = loader.loadModel(new FileHandle(new File(path)));
        Model black_piece = loader.loadModel(new FileHandle(new File(path)));
        black_piece.materials.get(0).set(ColorAttribute.createDiffuse(Color.BLACK));
        chess_pieces.put("W-" + piece_name, piece);
        chess_pieces.put("B-" + piece_name, black_piece);
    }

	@Override
	public void create() 
    {
        // Load the native opencv library
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        // Setup the video capture device and output image matrices
        cap = new VideoCapture();
        image = new Mat();
       	bw_image = new Mat();
       	thres_image = new Mat();
       	contour_image = new Mat();
       	polygon_image = new Mat();
       	cross_image = new Mat();
        // Open the video capture and grab a frame
        cap.open(0);
        // Setup resolution
        boolean wset = cap.set(Videoio.CV_CAP_PROP_FRAME_WIDTH, cam_width);
        boolean hset = cap.set(Videoio.CV_CAP_PROP_FRAME_HEIGHT, cam_height);
        /*
        if (wset == false || hset == false)
        {
            System.err.println("Unable to set resolution");
            System.exit(1);
        }
        */
        // Print out resolution
        cam_width = (float) cap.get(Videoio.CV_CAP_PROP_FRAME_WIDTH);
        cam_height = (float) cap.get(Videoio.CV_CAP_PROP_FRAME_HEIGHT);

        System.out.println("Camera is utilizing resolution: " + cam_width + "x" + cam_height);

        // Acquire a frame
        cap.read(image);
        // Test that the frame was valid
        while(image.type() == 0)
        {
            System.err.println("Image type invalid?");
            System.exit(1);
        }

        // Setup the environment
		environment = new Environment();
		environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
		environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));
        // Setup 3D rendering structure
		modelBatch = new ModelBatch();
        instances = new Array<Array<ModelInstance>>();
        cams = new ArrayList<PerspectiveOffCenterCamera>();
        // Setup 3D models
        ModelBuilder modelBuilder = new ModelBuilder();
        arrowX = modelBuilder.createArrow(origin, vecX, matRed, Usage.Position | Usage.Normal);
        arrowY = modelBuilder.createArrow(origin, vecY, matGreen, Usage.Position | Usage.Normal);
        arrowZ = modelBuilder.createArrow(origin, vecZ, matBlue, Usage.Position | Usage.Normal);
        // Chess pieces
        Map<String, Model> chess_pieces = new HashMap<String, Model>();
        loadChessPiece("Pawn.g3db", "Pawn");
        loadChessPiece("Bishop.g3db", "Bishop");
        loadChessPiece("Knight.g3db", "Knight");
        loadChessPiece("Rook.g3db", "Rook");
        loadChessPiece("Queen.g3db", "Queen");
        loadChessPiece("King.g3db", "King");
	}
	
    // Setup the camera
	public PerspectiveOffCenterCamera make_camera()
	{
        PerspectiveOffCenterCamera c = new PerspectiveOffCenterCamera();
        Mat intrinsics = UtilAR.getDefaultIntrinsics(cam_width, cam_height);

        c.setByIntrinsics(intrinsics, cam_width, cam_height);
		c.position.set(new Vector3(5f,5f,5f));
		c.lookAt(0f, 0f, 0f);
		c.near = 1f;
		c.far = 500f;
		c.update();
		cams.add(c);

        return c;
	}

	@Override
	public void render() 
    {
        // Clean up various bits
        cams.clear();
        instances.clear();

        // Capture a new image
        cap.read(image);

        //--------------------------
		// a) Create a binary image.
        //--------------------------
		// Grayscale it
		Imgproc.cvtColor(image, bw_image, Imgproc.COLOR_BGR2GRAY);
		//UtilAR.imShow("B/W", bw_image);

		// Threshold it
		Imgproc.threshold(bw_image, thres_image, 100.0f, 255.0f, Imgproc.THRESH_BINARY);
		//UtilAR.imShow("THRESHOLD", thres_image);
		
        //----------------------
		// b) Find the contours.
        //----------------------
		List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
		Imgproc.findContours(thres_image, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
        /*
        contour_image = image.clone();
		Imgproc.drawContours(contour_image, contours, -1, new Scalar(0,0,255));
		UtilAR.imShow("CONTOUR-COLOR", contour_image);
		*/

        //----------------------------------
		// c) Create approximative polygons.
        //----------------------------------
		List<MatOfPoint> polygons = new ArrayList<MatOfPoint>();
		for(MatOfPoint contour : contours)
		{
			MatOfPoint2f thisContour2f = new MatOfPoint2f();
		    MatOfPoint approxContour = new MatOfPoint();
		    MatOfPoint2f approxContour2f = new MatOfPoint2f();
			
		    contour.convertTo(thisContour2f, CvType.CV_32FC2);
		    Imgproc.approxPolyDP(thisContour2f, approxContour2f, 5, true);
		    approxContour2f.convertTo(approxContour, CvType.CV_32S);
			
			polygons.add(approxContour);
		}
        /*
        polygon_image = image.clone();
		Imgproc.drawContours(polygon_image, polygons, -1, new Scalar(0,255,0));
		UtilAR.imShow("POLY-COLOR", polygon_image);
		*/

        //-----------------------------------------------------
		// d) Discard polygons which are not marker candidates.
        //-----------------------------------------------------
        // Check that we're provided rectangles and that they are convex.
		List<MatOfPoint> squares = new ArrayList<MatOfPoint>();
		for(MatOfPoint polygon : polygons)
		{
			if(polygon.size().height == 4 && Imgproc.isContourConvex(polygon)) {
				squares.add(polygon);
			}
		}
        // Sort the squares by size, largest first
        Collections.sort(squares, new Comparator<MatOfPoint>() 
        {
            public int compare(MatOfPoint p1, MatOfPoint p2) 
            {
                double p1area = Imgproc.contourArea(p1);
                double p2area = Imgproc.contourArea(p2);
                // TODO:
                // Return p1area - p2area ?
                if(p1area > p2area)
                    return -1;
                else if(p1area < p2area)
                    return 1;
                else
                    return 0;
            }
        });
		// Let's only get squares which are comparatively big
		List<MatOfPoint> areasquares = new ArrayList<MatOfPoint>();
		if(squares.isEmpty() == false)
		{
			double biggest = Imgproc.contourArea(squares.get(0));
			for(MatOfPoint square : squares)
			{
				double current = Imgproc.contourArea(square);
				if(current > 0.5*biggest)
				{
					areasquares.add(square);
				}
			}
		}
		
        // Removes outer box border,
        // Only use the inner border.
		List<MatOfPoint> crosspsquares = new ArrayList<MatOfPoint>();
		if(squares.isEmpty() == false)
		{
			for(MatOfPoint square : areasquares)
			{
				double[] corner1 = square.get(0, 0);
				double[] corner2 = square.get(1, 0);
				double[] corner3 = square.get(2, 0);
				double a = corner1[0]-corner2[0];
				double x = corner3[0]-corner2[0];
				double b = corner1[1]-corner2[1];
				double y = corner3[1]-corner2[1];
				double crossp = a*y-b*x;
				if(crossp > 0)
				{
					crosspsquares.add(square);
				}

			}
		}
        //-----------------------------------------------------------------
		// e) For each marker candidate draw the four edges and/or corners.
        //      -- Display the result image.
        //-----------------------------------------------------------------
        /*
        cross_image = image.clone();    
        Imgproc.drawContours(cross_image, crosspsquares, -1, new Scalar(255,0,0));
        UtilAR.imShow("SQUARE-COLOR", cross_image);
        */
		
		int i = 0;
		for(MatOfPoint mat_square : crosspsquares)
		{
			MatOfPoint2f square = new MatOfPoint2f();
			mat_square.convertTo(square, CvType.CV_32FC2);
			
            //------------------------------------------------------------------------------------    
			// g) Unwarp the content of the found marker candidate and display the unwarped image.
            //------------------------------------------------------------------------------------
			List<Point> homepoints = new ArrayList<Point>();
			homepoints.add(new Point(0,0));
			homepoints.add(new Point(image.width(),0));
			homepoints.add(new Point(image.width(),image.height()));
			homepoints.add(new Point(0,image.height()));
	
			MatOfPoint2f homopoints = new MatOfPoint2f();
			homopoints.fromList(homepoints);
			
			Mat M = Calib3d.findHomography(square, homopoints);
			Mat dest = new Mat();
			Imgproc.warpPerspective(image, dest, M, image.size());
			
            //UtilAR.imClose("OUTPUT-COLOR" + i);
			//UtilAR.imShow("OUTPUT-COLOR" + i, dest);

			
			try
			{
                // Convert image to gray-scale
                Imgproc.cvtColor(dest, dest, Imgproc.COLOR_RGB2GRAY, 0);

                // Create an empty image in matching format
                BufferedImage gray = new BufferedImage(dest.width(), dest.height(), BufferedImage.TYPE_BYTE_GRAY);

                // Get the BufferedImage's backing array and copy the pixels directly into it
                byte[] data = ((DataBufferByte) gray.getRaster().getDataBuffer()).getData();
                dest.get(0, 0, data);

                // Convert to zxing's format
				LuminanceSource source = new BufferedImageLuminanceSource(gray);
				BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
                /* // XXX: Debug output ZXING Binary Image
                BitMatrix bm = bitmap.getBlackMatrix();
                Mat m = new Mat(bm.getHeight(), bm.getWidth(), CvType.CV_8UC1);
                for(int x=0; x<bm.getHeight(); x++)
                {
                    for(int y=0; y<bm.getWidth(); y++)
                    {
                        boolean set = bm.get(y, x);
                        int value = set ? 0 : 255;
                        m.put(x,y,value);
                    }
                }
                UtilAR.imShow("ZXING-DEBUG" + i, m);
                */

				Reader reader = new QRCodeReader();
				Result result = reader.decode(bitmap);
				System.out.println("QR-Code text is " + result.getText());
				String text = result.getText();
				
                // We've actually found something worth rendering, setup renderings infrastructure
                i++;
                Array<ModelInstance> models = new Array<ModelInstance>();
                instances.add(models);
                PerspectiveOffCenterCamera cam = make_camera();

                // Add the rendering stuff
                models.add(new ModelInstance(arrowX));
                models.add(new ModelInstance(arrowY));
                models.add(new ModelInstance(arrowZ));

                Model piece = chess_pieces.get(text);
                if(piece != null)
                {
                    ModelInstance piece_instance = new ModelInstance(piece);
                    piece_instance.transform.translate(0.5f,0f,0.5f);
                    piece_instance.transform.scale(0.1f,0.1f,0.1f);
                    models.add(piece_instance);
                }

                //-------------------------------------------------------------------------------------
                // f) Use the PnP solver to render the 3D coordinate system onto the marker candidates.
                //-------------------------------------------------------------------------------------
                /* INVERTED POINTS
				Point3[] points = { 
                    new Point3(1,0,0),
                    new Point3(1,0,1),
                    new Point3(0,0,1),
                    new Point3(0,0,0)
                };
                */
                Point3[] points = { 
                    new Point3(0,0,0),
                    new Point3(0,0,1),
                    new Point3(1,0,1),
                    new Point3(1,0,0)
				};
                MatOfPoint3f points3d = new MatOfPoint3f(points);

                Mat rvec = new Mat();
                Mat tvec = new Mat();
                Calib3d.solvePnP(points3d, square, UtilAR.getDefaultIntrinsics(cam_width, cam_height),
                        UtilAR.getDefaultDistortionCoefficients(), rvec, tvec);
                // Setup our camera 
                UtilAR.setCameraByRT(rvec, tvec, cam);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}

        // Render the raw image
		UtilAR.imDrawBackground(image);
		// Render all the different worlds onto it
		for(int j=0; j<i; j++)
		{
            // Loop through each camera, and using it draw the instaces on it
			modelBatch.begin(cams.get(j));
			modelBatch.render(instances.get(j), environment);
			modelBatch.end();
		}
	}

	@Override
	public void resize(int width, int height) 
    {
	}
	
	@Override
	public void pause() 
    {
	}

	@Override
	public void dispose() 
	{
		cap.release();
	}
	
	@Override
	public void resume() 
    {
	}
}
