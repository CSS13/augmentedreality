#!/bin/bash

source qr_config.sh

INPUT_FOLDER=QR_CODES
OUTPUT_FOLDER=QR_CODES_BORDER

# Make output folder
mkdir -p $OUTPUT_FOLDER
# Add border to QR codes
for piece in "${PIECES[@]}"; do
    for color in "${COLORS[@]}"; do
        echo "Bordering $color - $piece"

        FILE=$INPUT_FOLDER/$color-$piece.png
        OUTPUT_FILE=$OUTPUT_FOLDER/$color-$piece.png
        ## TODO: Check that files exist
        convert $FILE -bordercolor Black -border ${BORDER}x${BORDER} $OUTPUT_FILE
    done
done
