#!/bin/bash

source qr_config.sh

# Size of the chess board (BOARDSIZE x BOARDSIZE)
BOARD_SIZE=8
# Padding size is the margin around the QR code to the chess square
PADDING_SIZE=100
# The size of a single square on the board
SQUARE_SIZE=$((SIZE + 2*BORDER + PADDING_SIZE))
# Image size is determined to be large enough to fit (embed) all qr codes
IMAGE_SIZE=$((SQUARE_SIZE*BOARD_SIZE))
# Officers line setup
OFFICERS=(Rook Knight Bishop King Queen Bishop Knight Rook)
# Offsets for placement, place halfway through padding
Y_OFFSET=$((PADDING_SIZE/2))
X_OFFSET=$((PADDING_SIZE/2))

INPUT_FOLDER=QR_CODES_BORDER
OUTPUT_FOLDER=QR_CHESS_BOARD
WORK_IMAGE=$OUTPUT_FOLDER/work.png

# Make output folder
mkdir -p $OUTPUT_FOLDER
# Make initial chess board
convert -size ${BOARD_SIZE}x${BOARD_SIZE} pattern:gray50 -scale $((IMAGE_SIZE / BOARD_SIZE * 100))% $OUTPUT_FOLDER/chessboard.png
cp $OUTPUT_FOLDER/chessboard.png $WORK_IMAGE

# Add QR codes to chess board
for i in {0,1}; do
    COLOR=${COLORS[$i]}
    # Place officer line
    for x in {0..7}; do
        PIECE=${OFFICERS[$x]}
        FILE=$INPUT_FOLDER/$COLOR-$PIECE.png
        echo "Placing $COLOR-$PIECE".
        #echo "$FILE"
        #echo "$WORK_IMAGE"
        convert $WORK_IMAGE $FILE -geometry +$((SQUARE_SIZE*x+X_OFFSET))+$((i*7*SQUARE_SIZE+Y_OFFSET)) -compose copy -composite $WORK_IMAGE
    done
    # Place pawn line
    for x in {0..7}; do
        PIECE=Pawn
        FILE=$INPUT_FOLDER/$COLOR-$PIECE.png
        echo "Placing $COLOR-$PIECE".
        #echo "$FILE"
        #echo "$WORK_IMAGE"
        convert $WORK_IMAGE $FILE -geometry +$((SQUARE_SIZE*x+X_OFFSET))+$((i*5*SQUARE_SIZE+SQUARE_SIZE+Y_OFFSET)) -compose copy -composite $WORK_IMAGE
    done
done
# Copy file to output
cp $WORK_IMAGE $OUTPUT_FOLDER/output.png
