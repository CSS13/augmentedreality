#!/bin/bash

source qr_config.sh

OUTPUT_FOLDER=QR_CODES

# Make output folder
mkdir -p $OUTPUT_FOLDER
# Get QR codes
for piece in "${PIECES[@]}"; do
    for color in "${COLORS[@]}"; do
        echo "Generating $color - $piece"

        URL="https://zxing.org/w/chart?cht=qr&chs=${SIZE}x${SIZE}&chld=H&choe=UTF-8&chl=$color-$piece"
        echo "$URL"
        wget -O $OUTPUT_FOLDER/$color-$piece.png $URL
    done
done

