# Size of the generated QR codes
SIZE=500
# Black border thickness around QR codes
BORDER=50
# Piece colors
COLORS=(B W)
# Pieces
PIECES=(King Queen Rook Bishop Knight Pawn)
